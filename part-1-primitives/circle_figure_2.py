#!/usr/bin/python3.4
# circle_figure_2.py

from common import *

# --8<-- generate

def circle_points(N):
    n = numpy.arange(-N//2, N//2)
    p = numpy.exp(TWO_PI_J * n / N)
    x = p.real
    y = p.imag
    return x, y

# --8<--

with plot_figure('circle_figure_2.png') as fig:
    ax = fig.add_subplot(1, 1, 1)

    # --8<-- plot

    x, y = circle_points(N=360)
    ax.plot(x, y, **linestyle(1, label='N=360', marker=None))

    x, y = circle_points(N=8)
    ax.plot(x, y, **linestyle(2, label='N=8'))
    
    # --8<--

    ax.set_xlabel(
        r'real = $x$ = I = in-phase = $\cos(2\pi j\frac{n}{N} )$',
        labelpad=0)
    ax.set_ylabel(
        r'imag = $y$ = Q = quadrature = $j \sin(2\pi j\frac{n}{N} )$')

    ax.set_xlim([ -1.5, 1.5 ])
    ax.set_ylim([ -1.5, 1.5 ])
    ax.set_aspect('equal')

    ax.legend()

#--#
