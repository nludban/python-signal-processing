#!/usr/bin/python3.4
# sdequiv_figure_2.py

from common import *

# Analog
Na = 200
an = numpy.arange(-Na//2, Na//2) * 0.1
a1 = sinusoid(Na, 2).real
a2 = sinusoid(Na, 10).real
a3 = (sinusoid(Na, 5) * numpy.exp(TWO_PI_J * 0.125)).real


# Digital
Nd = 20
dn = numpy.arange(-Nd//2, Nd//2)
d1 = sinusoid(Nd, 2).real
d2 = sinusoid(Nd, 10).real
d3 = (sinusoid(Nd, 5) * numpy.exp(TWO_PI_J * 0.125)).real

# --8<-- reconstruct

def reconstruct(d):
    r = numpy.zeros(Na)
    for delay, scale in zip(dn, d):
        r += scale * sinc(an, delay)
    return r

# --8<--

with plot_figure('sdequiv_figure_2.png') as fig:

    ax = fig.add_subplot(3, 1, 1)
    ax.plot(an, a1, **linestyle(0))
    ax.plot(dn, d1, **linestyle(3, linestyle='',
                                marker='o', markersize=8))
    ax.plot(an, reconstruct(d1), **linestyle(1, marker=None))
    ax.get_xaxis().set_ticks([])
    ax.set_ylim([-1.2, 1.2])

    ax = fig.add_subplot(3, 1, 2)
    ax.plot(an, a2, **linestyle(0))
    ax.plot(dn, d2, **linestyle(3, linestyle='',
                                marker='o', markersize=8))
    ax.plot(an, reconstruct(d2), **linestyle(1, marker=None))
    ax.get_xaxis().set_ticks([])
    ax.set_ylim([-1.2, 1.2])

    ax = fig.add_subplot(3, 1, 3)
    ax.plot(an, a3, **linestyle(0))
    ax.plot(dn, d3, **linestyle(3, linestyle='',
                                marker='o', markersize=8))
    ax.plot(an, reconstruct(d3), **linestyle(1, marker=None))
    ax.set_ylim([-1.2, 1.2])


#--#
