#!/usr/bin/python3.4

from common import *

# --8<-- generate

def sinusoid(N, F):
    n = numpy.arange(-N//2, N//2)
    s = numpy.exp(TWO_PI_J * F * n/N)
    return s

# --8<--

with plot_figure('frequency_figure_1.png') as fig:

    # --8<-- plot

    N = 8
    n = numpy.arange(-N//2, N//2)
    s = [ sinusoid(N, F) for F in range(8) ]

    ax = fig.add_subplot(2, 5, 1)
    ax.plot(n, s[0].real, **linestyle('1I', label='I'))
    ax.plot(n, s[0].imag, **linestyle('1Q', label='Q'))
    ax.get_xaxis().set_ticks([])
    #ax.get_yaxis().set_ticks([])
    ax.set_title('F = 0')

    # --8<--

    ax = fig.add_subplot(2, 5, 2)
    ax.plot(n, s[1].real, **linestyle('1I', label='I'))
    ax.plot(n, s[1].imag, **linestyle('1Q', label='Q'))
    ax.get_xaxis().set_ticks([])
    ax.get_yaxis().set_ticks([])
    ax.set_title('F = 1')

    ax = fig.add_subplot(2, 5, 3)
    ax.plot(n, s[2].real, **linestyle('1I', label='I'))
    ax.plot(n, s[2].imag, **linestyle('1Q', label='Q'))
    ax.get_xaxis().set_ticks([])
    ax.get_yaxis().set_ticks([])
    ax.set_title('F = 2')

    ax = fig.add_subplot(2, 5, 4)
    ax.plot(n, s[3].real, **linestyle('1I', label='I'))
    ax.plot(n, s[3].imag, **linestyle('1Q', label='Q'))
    ax.get_xaxis().set_ticks([])
    ax.get_yaxis().set_ticks([])
    ax.set_title('F = 3')

    ax = fig.add_subplot(2, 5, 7)
    ax.plot(n, s[7].real, **linestyle('1I', label='I'))
    ax.plot(n, s[7].imag, **linestyle('1Q', label='Q'))
    #ax.get_xaxis().set_ticks([])
    #ax.get_yaxis().set_ticks([])
    ax.set_title('F = 7')

    ax = fig.add_subplot(2, 5, 8)
    ax.plot(n, s[6].real, **linestyle('1I', label='I'))
    ax.plot(n, s[6].imag, **linestyle('1Q', label='Q'))
    #ax.get_xaxis().set_ticks([])
    ax.get_yaxis().set_ticks([])
    ax.set_title('F = 6')

    ax = fig.add_subplot(2, 5, 9)
    ax.plot(n, s[5].real, **linestyle('1I', label='I'))
    ax.plot(n, s[5].imag, **linestyle('1Q', label='Q'))
    #ax.get_xaxis().set_ticks([])
    ax.get_yaxis().set_ticks([])
    ax.set_title('F = 5')

    ax = fig.add_subplot(2, 5, 10)
    ax.plot(n, s[4].real, **linestyle('1I', label='I'))
    ax.plot(n, s[4].imag, **linestyle('1Q', label='Q'))
    #ax.get_xaxis().set_ticks([])
    ax.get_yaxis().set_ticks([])
    ax.set_title('F = 4')


#--#
