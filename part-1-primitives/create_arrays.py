#!/usr/bin/python3.4
# create_arrays.py

from common import *

#### Creating numpy arrays

# --8<-- examples

## From any iterable
print(numpy.array([ 10, 20, 30, 40, 50 ]), '\n')

## Similar to list(range(N))
print(numpy.arange(5), '\n')

## Other
print(numpy.zeros(5))
print(numpy.ones(5))

# --8<--

#--#
