#!/usr/bin/python3.4
# delta_figure_1.py

from common import *

N = 20
n = numpy.arange(-N//2, N//2)

with plot_figure('delta_figure_1.png') as fig:

    ax = fig.add_subplot(3, 1, 1)
    ax.stem(n, delta(n, 0), **linestyle(1))
    ax.get_xaxis().set_ticks([])
    ax.set_ylim([-1.2, 1.2])

    # --8<-- generate_1
    y1 = delta(n, 0)
    y2 = 0.3 * delta(n, 3)
    y3 = -0.8 * delta(n, -7)
    # --8<--

    ax = fig.add_subplot(3, 1, 2)
    ax.stem(n, y1, **linestyle(1))
    ax.stem(n, y2, **linestyle(2))
    ax.stem(n, y3, **linestyle(3))
    ax.get_xaxis().set_ticks([])
    ax.set_ylim([-1.2, 1.2])

    # --8<-- generate_2
    y = y1 + y2 + y3
    # --8<--

    ax = fig.add_subplot(3, 1, 3)
    ax.stem(n, y, **linestyle(1))
    ax.set_ylim([-1.2, 1.2])


#--#
