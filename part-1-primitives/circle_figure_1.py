#!/usr/bin/python3.4
# circle_figure_1.py

from common import *

# --8<-- generate

d = numpy.linspace(0, 360, 361)
x = numpy.cos(d * TWO_PI / 360)
y = numpy.sin(d * TWO_PI / 360)

# --8<--

with plot_figure('circle_figure_1.png') as fig:
    ax = fig.add_subplot(1, 1, 1)

    ax.plot(x, y, **linestyle(1, marker=None))
    ax.set_xlabel('$\cos{d}$')
    ax.set_ylabel('$\sin{d}$')

    ax.set_xlim([ -1.5, 1.5 ])
    ax.set_ylim([ -1.5, 1.5 ])
    ax.set_aspect('equal')

#--#
