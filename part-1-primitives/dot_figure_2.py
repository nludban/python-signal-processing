#!/usr/bin/python3.4
# dot_figure_2.py

from common import *


N = 64
scale = 2 / N # 1/N for complex input; 2/N for real
h3 = sinusoid(N, 3).real * scale

s3 = sinusoid(N, 3).real
s11 = sinusoid(N, 11).real

# --8<-- generate

s = 1.4 * s3 - 0.8 * s11

# --8<--

n = numpy.arange(-N//2, N//2)

with plot_figure('dot_figure_2.png') as fig:

    ax = fig.add_subplot(1, 1, 1)
    ax.axhline(0, color='black')
    ax.axvline(0, color='black')
    ax.plot(n, s, **linestyle(1))
    ax.set_ylabel(r'$1.4 s_3 - 0.8 s_{11}$')

# --8<-- test

print('numpy.dot(s, h3) =', numpy.dot(s, h3))

# --8<--

#--#
