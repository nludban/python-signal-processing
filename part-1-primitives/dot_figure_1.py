#!/usr/bin/python3.4

from common import *


# --8<-- generate

N = 64
scale = 2 / N # 1/N for complex input; 2/N for real
h3 = sinusoid(N, 3).real * scale

s3 = sinusoid(N, 3).real
s11 = sinusoid(N, 11).real

# --8<--

n = numpy.arange(-N//2, N//2)

with plot_figure('dot_figure_1.png') as fig:

    ax = fig.add_subplot(3, 1, 1)
    ax.axhline(0, color='black')
    ax.axvline(0, color='black')
    ax.plot(n, h3, **linestyle(1))
    ax.set_ylabel(r'$h_3$')

    ax = fig.add_subplot(3, 1, 2)
    ax.axhline(0, color='black')
    ax.axvline(0, color='black')
    ax.plot(n, s3, **linestyle(1))
    ax.set_ylabel(r'$s_3$')

    ax = fig.add_subplot(3, 1, 3)
    ax.axhline(0, color='black')
    ax.axvline(0, color='black')
    ax.plot(n, s11, **linestyle(1))
    ax.set_ylabel(r'$s_{11}$')

# --8<-- test

print('numpy.dot(s3, h3) =', numpy.dot(s3, h3))
print('numpy.dot(s11, h3) =', numpy.dot(s11, h3))
print('numpy.dot(s3 * 1.4, h3) =', numpy.dot(s3 * 1.4, h3))
print('numpy.dot(s3 * -0.8, h3) =', numpy.dot(s3 * -0.8, h3))
print('numpy.dot(s11 * -42, h3) =', numpy.dot(s11 * -42, h3))

# --8<--

#--#
