#!/usr/bin/python3.4
## basic_figure_2.py

from common import *

# --8<-- generate

x1 = numpy.arange(10)
y1 = numpy.sqrt(x1)

x2 = numpy.arange(0, 10, 3)
y2 = numpy.sqrt(x2)

x3 = numpy.arange(0, 10, 0.1)
y3 = numpy.sqrt(x3)

# --8<--

with plot_figure('basic_figure_2.png') as fig:

    # Divide the figure into 1x1 subplots, get the 1st axes
    ax = fig.add_subplot(1, 1, 1)

    ax.set_title('The plot title')
    ax.set_xlabel('The X axis label')
    ax.set_ylabel('The Y axis label')

    ax.axhline(0, color='black')
    ax.axvline(0, color='black')

# --8<-- plot

    ax.plot(x1, y1, **linestyle(1, label=r'$y_1 = \sqrt{x}$'))

    ax.plot(x2, y2, **linestyle(2, label=r'$y_2 = \sqrt{x}$'))

    ax.plot(x3, y3, **linestyle(3, label=r'$y_3 = \sqrt{x}$',
                                marker=None))
# --8<--

    ax.set_xlim([ -1, 11 ])
    ax.set_ylim([ -1, 4 ])
    ax.grid(True)
    ax.legend()

# --8<--


#--#
