#!/usr/bin/python3.4

import math
import os
import sys

#### Imports for numpy and plotting.

# --8<-- imports

import numpy
import numpy.fft
import numpy.random

import matplotlib.pyplot

## Commonly seen import aliases:
#
#import numpy as np
#import matplotlib.pyplot as plt

## Not used today:
#
#import scipy
#import scipy as sp

# --8<--

#### Older matplotlib API that merged pyplot and numpy.
#### Not recommended.

#import pylab


#### Useful constants

TWO_PI		= math.pi * 2
TWO_PI_J	= math.pi * 2j


#### Utility functions.

def print_array(label, arr, **kwargs):
    indent = ' ' * (len(label.lstrip('\n')) + 1)
    return print(label,
                 str(arr).replace('\n', '\n' + indent),
                 **kwargs)


import contextlib
# --8<-- plot_figure

@contextlib.contextmanager
def plot_figure(filename_ext, width=640, height=480, dpi=100):
    fig = matplotlib.pyplot.figure()
    fig.set_size_inches(width / dpi, height / dpi)
    yield fig
    fig.savefig(filename_ext, dpi=dpi)
    return

# --8<--

def linestyle(index, **kwargs):
    kwargs.setdefault('label', 'Line %s' % index)
    if isinstance(index, str):
        index, channel = int(index[0]), index[1]
    else:
        channel = '-'
    assert channel in ( '-', 'I', 'Q' )
    assert 0 <= index <= 3
    #index -= 1 // 0 = analog (gray)

    COLORS = [ '#888888', 'red', 'green', 'blue' ]
    kwargs.setdefault('color', COLORS[index])

    MARKERS = [ None, 'o', 's', 'x' ]
    kwargs.setdefault('marker', MARKERS[index])

    LINESTYLES = { '-' : '-', 'I' : '-', 'Q' : ':' }
    kwargs.setdefault('linestyle', LINESTYLES[channel])

    kwargs.setdefault('linewidth', 6 if index == 0 else 2)
    kwargs.setdefault('markersize', 6)

    return kwargs


# --8<-- sinusoid

def sinusoid(N, F):
    n = numpy.arange(-N//2, N//2)
    s = numpy.exp(TWO_PI_J * F * n/N)
    return s

# --8<--


# --8<-- delta

def delta(t, t0):
    # y[t] = 1 where t == t0, otherwise 0
    y = numpy.zeros_like(t)
    y[t - t0 == 0] = 1
    return y

# --8<--

# Deal with FP imprecision?
#def delta(n, k):
#    y = numpy.zeros_like(n)
#    y[numpy.abs(n - k) < 1e-6] = 1
#    return y


# --8<-- sinc

def sinc(t, t0):
    y = numpy.sinc(t - t0)
    return y

# --8<--

#--#
