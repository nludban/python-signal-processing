#!/usr/bin/python3.4

from common import *

#### Code extracted from explots.py to create png:

with plot_figure('speedup_figure_1.png') as fig:

    n = [ 100, 1000, 10e3, 100e3, 1e6, 5e6 ]
    lt = numpy.array(
        [ 0.000976, 0.007959, 0.078368, 0.820413, 8.163863, 41.388439 ])
    nt = numpy.array(
        [ 0.000458, 0.000946, 0.006032, 0.057410, 0.554600, 2.776934 ])

    ax = fig.add_subplot(2, 1, 1)
    ax.set_title('Relative Performance')
    ax.loglog(n, lt, 'r+-')
    ax.loglog(n, nt, 'g+-')
    ax.set_ylim(-10, 50)
    ax.set_ylabel('seconds')
    ax = fig.add_subplot(2, 1, 2)
    ax.semilogx(n, lt / nt, 'k+-')
    ax.set_ylabel('speedup')
    ax.set_xlabel('N')

#--#
