#!/usr/bin/python3.4
# convolve_figure_1.py

from common import *

N = 800
n = numpy.arange(-N//2, N//2)

s = sinusoid(N, F=100).real

Nh = 48 # 6 cycles x 800/100 samples/cycle
hn = numpy.arange(-Nh//2, Nh//2)
scale = (2/Nh)
a = N//2 - Nh//2
b = N//2 + Nh//2
h = s[a:b] * scale

s[0:96] = 0
s[144:256] = 0
s[324:640] = 0
s[672:] = 0

# Add some random noise
s += numpy.random.normal(0, 0.1, N)

# --8<-- convolve

def convolve(x, h):
    Nx = x.shape[0]
    h = h[::-1].copy() # reversed
    Nh = h.shape[0]
    y = numpy.zeros_like(x)
    for n in range(Nh//2, Nx - Nh//2):
        a = n - Nh//2
        b = a + Nh
        y[n] = numpy.dot(s[a:b], h)
    return y

# --8<--

with plot_figure('convolve_figure_1.png') as fig:

    ax = fig.add_subplot(3, 1, 1)
    ax.plot(hn, h, **linestyle(1, marker=None))
    ax.set_ylabel('find me')
    ax.set_ylim([ -0.05, 0.05 ])

    ax = fig.add_subplot(3, 1, 2)
    ax.plot(n, s, **linestyle(1, marker=None))
    ax.set_ylabel('input')
    ax.get_xaxis().set_ticks([])
    ax.set_ylim([ -2, 2 ])

    y = convolve(s, h)

    ax = fig.add_subplot(3, 1, 3)
    ax.plot(n, y, **linestyle(1, marker=None))
    ax.set_ylabel('output')
    ax.set_ylim([ -2, 2 ])

#--#
