#!/usr/bin/python3.4
# create_matrices.py

from common import *

#### Creating numpy "matrices"

# --8<-- examples

## From any iterable
print(numpy.array([ [ 11, 12, 13, 14, 15 ],
                    [ 21, 22, 23, 24, 25 ],
                    [ 31, 32, 33, 34, 35 ] ]), '\n')

## Other
print(numpy.zeros(shape=( 3, 5 )))
print(numpy.ones(( 3, 5 )))

# --8<--

#--#
