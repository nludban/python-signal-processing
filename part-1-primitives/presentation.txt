#######################################################################
presentation.txt
#######################################################################
:title: Signal Processing Primitives; Illustrated With Python
:summary: Using Python to show the basics of Digital Signal Processing.



***********************************************************************
Title Page
***********************************************************************

.. topic:: Signal Processing Primitives
	:name: title

	Illustrated By Python

	\https://bitbucket.org/nludban/python-signal-processing/

	Presented 2016-07-25,
	The Central Ohio Python User's Group
	`<http://www.cohpy.org/>`_


=======================================================================
Introduction
=======================================================================

-----------------------------------------------------------------------
A very short presentation on processing signals with Python.
-----------------------------------------------------------------------

* `numpy <http://www.numpy.org/>`_ is used for data manipulation

  .. code:: python

     >>> len(dir(numpy))
     583

* `matplotlib <http://matplotlib.org/>`_ is used for plotting

  .. code:: python

     >>> len(dir(matplotlib.pyplot))
     243

* Both borrow heavily from `MATLAB <http://www.mathworks.com/products/matlab/>`_

  * Good: MATLAB documentation and books are helpful
  * Bad: Got the Fortran smells, too


-----------------------------------------------------------------------
Primitives are applicable to many domains:
-----------------------------------------------------------------------

* Audio processing; Sonar; Ultrasound
* Image processing
* Video processing
* RF: CT, MRI, Radar
* Communications: WiFi, Cellular, GPS


-----------------------------------------------------------------------
Pure python is always going to be slow.  See also:
-----------------------------------------------------------------------

* numpy and `scipy <https://www.scipy.org/>`_ implement high level
  primitives with more efficient algorithms
* Build with faster math libraries:

  * `ATLAS <http://math-atlas.sourceforge.net/>`_
  * `MKL <https://software.intel.com/en-us/intel-mkl>`_

* `cython <http://cython.org/>`_ compiles python+numpy to C
* Offload / accelerate processing

  * `multiprocessing <https://docs.python.org/3/library/multiprocessing.html>`_
  * `PyCUDA <https://mathema.tician.de/software/pycuda/>`_
    and `cuBLAS <https://developer.nvidia.com/cublas>`_
  * `pyMIC <https://software.intel.com/en-us/articles/pymic-a-python-offload-module-for-the-intelr-xeon-phitm-coprocessor>`_


.. If you want to prove the equations:

  * algebra, linear algebra, trigonometry, calculus
  * computer science
  * electrical engineering


=======================================================================
Arrays in NumPy
=======================================================================

-----------------------------------------------------------------------
Common imports
-----------------------------------------------------------------------

.. code:: python

   @@include common.py imports


-----------------------------------------------------------------------
Create empty (or filled) vectors
-----------------------------------------------------------------------

.. code:: python

	@@include create_arrays.py examples

.. code:: python
	:class: output

	@@run create_arrays.py


-----------------------------------------------------------------------
Create empty (or filled) matrices
-----------------------------------------------------------------------

.. code:: python

	@@include create_matrices.py examples

.. code:: python
	:class: output

	@@run create_matrices.py


-----------------------------------------------------------------------
Simple Arithmetic Operations
-----------------------------------------------------------------------

* NumPy overloads the arithmetic operators
* Concise syntax (less code)
* Reads like algebra (mostly)

  * Python 3.5 adds matrix multiply @ operator (`PEP 465
    <https://www.python.org/dev/peps/pep-0465/>`_)

.. code:: python

	@@include simple_math.py examples

.. code:: python
	:class: output

	@@run simple_math.py


-----------------------------------------------------------------------
NumPy class ndarray
-----------------------------------------------------------------------

* A memory buffer (C pointer) holds many elements of one type
  (eg: numpy.int16, numpy.float64)
* An ndarray is a "view" of the memory given a requested shape

  * ndarray = N-Dimensional Array
  * shape = number of dimensions, size on each

    * vector = 1-dimensional
    * matrix = 2-dimensional

  * memory is shared until explicitly copied

* numpy.array() is a utility function for creating an ndarray


.. code:: python

	@@include array_views.py examples

.. code:: python
	:class: output

	@@run array_views.py

* See also a much bigger `tutorial <https://docs.scipy.org/doc/numpy-dev/user/quickstart.html>`_


-----------------------------------------------------------------------
Performance considerations
-----------------------------------------------------------------------

* Python lists are slow because

  * Looping is in Python
  * Every element can have a different type
  * Every element is dynamically allocated

* NumPy arrays are fast because

  * Looping is in C
  * Every element is the same type
  * Elements are (usually) contiguous in memory (cache friendly)
  * Optimizing compilers
  * Optimized libraries
  * `SIMD <https://en.wikipedia.org/wiki/SIMD#Hardware>`_
    CPU instructions (MMX / SSE / AVX)
  * More generally:

    * Many problems are expressable in Linear Algebra
    * Investments were made in software and hardware to make LA fast
    * More problems are expressed in LA
    * ...

* Example speedup

  * Python lists:

    .. code:: python

       x = [ ]
       y = [ ]
       for k in range(-N//2, N//2):
           phi = 3 * k / N
           x.append(k)
           y.append(math.cos(2 * math.pi * phi))

  * NumPy arrays:

    .. code:: python

       k = numpy.arange(N) - N//2
       phi = 3 * k / N
       y = numpy.cos(2 * math.pi * phi)

  * Profiled on a Raspberry Pi 3:

    .. figure:: speedup_figure_1.png
       :align: center

  * Manually reduce equations = another 15% faster

    .. code:: python

       k = numpy.arange(-N//2, N//2)
       phi = k * (2 * math.pi * 3 / N)
       y = numpy.cos(phi)


-----------------------------------------------------------------------
Coding considerations
-----------------------------------------------------------------------

* Object oriented design = encapsulate data
* NumPy = big multi-dimensional pile of data
* Extra effort to keep track of dimensions


=======================================================================
Basic Plotting
=======================================================================

-----------------------------------------------------------------------
Generate same data
-----------------------------------------------------------------------

* Two equivalent ways of generating data:

  .. code:: python

	@@include basic_figure_1.py generate

* numpy.sqrt is a "universal function" or "ufunc"

  * For scalar inputs, works like math.sqrt
  * For ndarray inputs:

    * Output array is same shape as input
    * Output elements are calculated by applying ufunc to corresponding
      individual input elements

* numpy has ufuncs for math module functions, and many more


-----------------------------------------------------------------------
Display the data
-----------------------------------------------------------------------

1) Create a new figure

   * see common.py utility context for matplotlib.pyplot.figure
   
2) Get axes for a subplot
3) Set plot title and axis labels
4) Manually add horizontal and vertical axis lines
5) Plot data on the axes
6) Manually set display limits, enable grid and legend

.. code:: python

	@@include basic_figure_1.py display

* See also `mathtext <http://matplotlib.org/users/mathtext.html>`_
  for latex-style equation support

.. figure:: basic_figure_1.png
	:align: center
	:alt: Explanatory text.
	:class: output

	Figure generated by basic_figure_1.py


-----------------------------------------------------------------------
Unrestricted domain
-----------------------------------------------------------------------

* What if the x axis values are not range(N)?
* Same equation (x1), also fewer (x2) and more (x3) points

  .. code:: python

	@@include basic_figure_2.py generate

  * See also numpy.linspace()

* Also pass x to plot(); it effectively does zip(x, y) to get the
  list of coordinates for each point

  .. code:: python

	@@include basic_figure_2.py plot

.. figure:: basic_figure_2.png
	:align: center
	:alt: Explanatory text.
	:class: output

	Figure generated by basic_figure_2.py


-----------------------------------------------------------------------
Unrestricted domain and range
-----------------------------------------------------------------------

* Equations like y = f(x) "move" left to right
* But plot() can connect the points in any direction
* Example: short line segments creating a circle

  .. code:: python

     @@include circle_figure_1.py generate

  .. figure:: circle_figure_1.png
     :align: center
     :class: output

     Figure generated by circle_figure_1.py


=======================================================================
Sampling Theory
=======================================================================

-----------------------------------------------------------------------
Analog to digital conversion
-----------------------------------------------------------------------

* An analog to digital converter (ADC) samples the input signal at
  fixed time intervals

  * Fs = sample frequency (Hz)
  * T = sample interval (seconds)

* When do the samples fail to represent the input?

.. figure:: adc_figure_1.png
   :align: center

   Figure generated by adc_figure_1.py


-----------------------------------------------------------------------
Delta function
-----------------------------------------------------------------------

* Definition

  .. code:: python

     @@include common.py delta

* Examples

  1) delta(0)
  2) various scaled and delayed

     .. code:: python

        @@include delta_figure_1.py generate_1

  3) sum of scaled and delayed

     .. code:: python

        @@include delta_figure_1.py generate_2

  .. figure:: delta_figure_1.png
     :align: center

     Figure generated by delta_figure_1.py


-----------------------------------------------------------------------
Sinc function
-----------------------------------------------------------------------

* Definition (numpy.sinc)

  .. math::

     \sin(\pi t)/(\pi t)

* With a time delay:

  .. code:: python

     @@include common.py sinc

* Examples

  1) sinc(0)
  2) various scaled and delayed

     .. code:: python

        @@include sinc_figure_1.py generate_1

  3) sum of scaled and delayed

     .. code:: python

        @@include sinc_figure_1.py generate_2

  .. figure:: sinc_figure_1.png
     :align: center

     Figure generated by sinc_figure_1.py


-----------------------------------------------------------------------
Equivalence tests
-----------------------------------------------------------------------

* Sinc(t, 0) and delta(t, 0) intersect

  .. figure:: sdequiv_figure_1.png
     :align: center

     Figure generated by sdequiv_figure_1.py

* More interesting signals:

  .. code:: python

     @@include sdequiv_figure_2.py reconstruct

  * Convert an input signal to a sum of scaled and delayed deltas
  * Sum similarly scaled and delayed sincs

  .. figure:: sdequiv_figure_2.png
     :align: center

     Figure generated by sdequiv_figure_2.py

  * Note artifacts near the edges, all ADC samples outside the
    input signal time interval are assumed zero.


=======================================================================
Imaginary Circles
=======================================================================

-----------------------------------------------------------------------
Complex exponentiation
-----------------------------------------------------------------------

* numpy.exp() calculates powers of `e <https://en.wikipedia.org/wiki/E_%28mathematical_constant%29>`_

  .. math::

     \exp(x) = e^{x}

     e \approx 2.71828

* If x is imaginary (complex number), so is the result

  * :math:`i = \sqrt{-1}`

* Python uses "j" instead of "i" to reduce accidents with the common
  variable name

  .. code:: python

     >>> 2.7j
     2.7j
     >>> 3-4j
     (3-4j)

-----------------------------------------------------------------------
Demonstration
-----------------------------------------------------------------------

.. code:: python

   @@include circle_figure_2.py generate

.. code:: python

   @@include circle_figure_2.py plot

.. figure:: circle_figure_2.png
   :align: center
   :class: output

   Figure generated by circle_figure_2.py


-----------------------------------------------------------------------
`Euler's identity <https://en.wikipedia.org/wiki/Euler%27s_identity>`_
-----------------------------------------------------------------------

* :math:`e^{\pi i} + 1 = 0`
* Verify in Python:

  .. code:: python

     >>> numpy.exp(math.pi * 1j) + 1
     1.2246467991473532e-16j

* Note the circle_points() endpoints

  * Starts at n = -N/2
  * Ends before n = +N/2
  * Read from the plot: I=-1, Q=0

* Algebra:

  .. math::

     \exp(2 \pi j * (N/2) * N) = -1+0j

     \exp(2 \pi j * (1/2)) = -1

     \exp(\pi j) +1 = 0


=======================================================================
Oscillators
=======================================================================

-----------------------------------------------------------------------
Complex sinusoids
-----------------------------------------------------------------------

* Equation similar to circle_points(), but multiplied by F

.. code:: python

   @@include common.py sinusoid

* F is "sort of" the frequency

  * `normalized frequency <https://en.wikipedia.org/wiki/Normalized_frequency_%28unit%29>`_
    = radians per sample = 2 * pi * F; common in DSP
  * `angular frequency <https://en.wikipedia.org/wiki/Angular_frequency>`_
    = radians per second = 2 * pi * f; f in Hz

* F is the number of steps around an N-point circle (counter-clockwise),
  per sample

  * F == N/2 (the `Nyquist frequency <https://en.wikipedia.org/wiki/Nyquist_frequency>`_)

    * Half way around, either direction

  * F < N/2

    * F = number of cycles (trips around the circle) in N samples

  * F > N/2

    * So-called "negative" frequencies
    * Number of cycles = N - F, clockwise


-----------------------------------------------------------------------
Plotted in the complex plane
-----------------------------------------------------------------------

.. code:: python

   @@include frequency_figure_2.py plot

.. figure:: frequency_figure_2.png
   :class: output

   Figure generated by frequency_figure_2.py

* Positive frequencies travel counter-clockwise
* Negative frequencies appear to travel clockwise

  * Mirror images, flipped up-down

* Note that F = 3 + k*N looks just like F = 3; for any integer k

-----------------------------------------------------------------------
Separate I and Q lines
-----------------------------------------------------------------------

* Generate all discrete (integer) frequencies for N=8

.. code:: python

   @@include frequency_figure_1.py plot

.. figure:: frequency_figure_1.png
   :class: output

   Figure generated by frequency_figure_1.py

* Note the similarities:

  * I (solid lines) is symmetric for positive and negative F
  * Q (dashed lines) is assymetric

* Remainder of the presentation will look at I (cosine) only

  * The symmetry is mathematically easier to work with
  * Next time: I and Q form a basis for all delays


=======================================================================
Correlation
=======================================================================

-----------------------------------------------------------------------
Problem description
-----------------------------------------------------------------------

* Given two signals, are they:

  * Identical
  * Scaled versions of each other
  * Nothing at all alike

* Use the `dot product <https://en.wikipedia.org/wiki/Dot_product>`_

  .. code:: python

     def dot_product(x, h):
         return sum(x * h)

  * x is the input signal
  * h is the reference

    * Often scaled to return 1 for input with amplitude 1

  * Return values:

    * Positive = similar
    * Negative = opposite
    * Zero = nothing alike

-----------------------------------------------------------------------
Demonstration
-----------------------------------------------------------------------

* Generate a reference and two input signals

.. code:: python

   @@include dot_figure_1.py generate

.. figure:: dot_figure_1.png
   :align: center
   :class: output

   Figure generated by dot_figure_1.py

* Calculate some dot products

.. code:: python

   @@include dot_figure_1.py test

.. code:: python
   :class: output

   @@run dot_figure_1.py


-----------------------------------------------------------------------
Multiple input signals
-----------------------------------------------------------------------

* What if the input is the sum of scaled signals of different frequency?

.. code:: python

   @@include dot_figure_2.py generate

.. figure:: dot_figure_2.png
   :align: center
   :class: output

   Figure generated by dot_figure_2.py

* Calculate the dot product

.. code:: python

   @@include dot_figure_2.py test

.. code:: python
   :class: output

   @@run dot_figure_2.py

* Linear operations:

  * Sum the inputs and correlate
  * Or correlate inputs and sum the outputs
  * Same end result

-----------------------------------------------------------------------
Test for all possible frequencies
-----------------------------------------------------------------------

* Build a list of h of references

  .. code:: python

     @@include dot_figure_3.py references

* Correlate the input, s, with each h:

  .. code:: python

     @@include dot_figure_3.py generate

* Plot the result

  * Upper case "S" is the frequency response of "s"
  * Output is an array indexed by "frequency bin", k

  .. figure:: dot_figure_3.png
     :align: center
     :class: output

     Figure generated by dot_figure_3.py

  * S[3] = 1.4
  * S[11] = -0.8
  * S[k] == S[-k]


-----------------------------------------------------------------------
Discrete Fourier Transform
-----------------------------------------------------------------------

* As above, except:

  .. code:: python

     scale = 1 / N # 1/N for complex input
     h = [ sinusoid(N, F) * scale
           for F in range(-N//2, N//2) ]

* Frequency bin k = 0 to N-1; not -N/2 to N/2
* Input can be real (I only) or complex (I and Q)
* Input is also shifted 0 to N-1
* Output is complex

  * I = correlate input with cos(Fk)
  * Q = correlate input with sin(Fk)

* Using trigonometry, amplitude and time delay / angle / phase of each
  input component can be recovered


-----------------------------------------------------------------------
Fast Fourier Transform
-----------------------------------------------------------------------

* DFT running time is O(N^2)

  * Each output bin is a dot product (N multiply-add ops)
  * N output bins

* FFT is an efficient implementation of the DFT

  * O(N log N)
  * https://en.wikipedia.org/wiki/Fast_Fourier_transform
  * fftw papers?

* TODO: add numpy.fft example code + plot


-----------------------------------------------------------------------
Inverse Fourier Transform
-----------------------------------------------------------------------

* TODO: given S[k], produce s...
* Also O(N log N)


-----------------------------------------------------------------------
FT Theory
-----------------------------------------------------------------------

* Visualizing system response as a function of frequency is useful
* System response to arbitrary inputs can be predicted

  * The input can be decomposed into a sum of sinusoids
  * System response to cosine is relatively easy to calculate

    * Output is another sinusoid (scaled and delayed, but same
      frequency)

  * Add an imaginary delay to the sine inputs, calculate as cosine,
    undo the delay on the output
  * Sum the outputs

* Many O(N^2) problems can be solved in frequency in O(N)

  * FFT + solve + IFFT
  * O(N log N) + O(N) + O(N log N) = O(N log N)


=======================================================================
Convolution
=======================================================================

-----------------------------------------------------------------------
Definition
-----------------------------------------------------------------------

* Similar to convolution, except:

  * The reference, h, is reversed
  * A "sliding window" of x is selected for each output

.. code:: python

   @@include convolve_figure_1.py convolve

* Various ways of dealing with end conditions; this code sets the
  end outputs to zero


-----------------------------------------------------------------------
Example
-----------------------------------------------------------------------

  .. figure:: convolve_figure_1.png
     :align: center
     :class: output

     Figure generated by convolve_figure_1.py

=======================================================================
The End.
=======================================================================

