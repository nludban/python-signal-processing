#!/usr/bin/python3.4
## basic_figure_1.py

from common import *

# --8<-- generate

## Slower:
y = numpy.zeros(10)
for k in range(10):
    y[k] = math.sqrt(k)

## Faster:
x = numpy.arange(10)
y = numpy.sqrt(x)

# --8<-- display
with plot_figure('basic_figure_1.png') as fig:

    # Divide the figure into 1x1 grid of subplots, get the first
    ax = fig.add_subplot(1, 1, 1)	# Note 111 also works

    ax.set_title('The plot title')
    ax.set_xlabel('The X axis label')
    ax.set_ylabel('The Y axis label')

    ax.axhline(0, color='black')
    ax.axvline(0, color='black')
    ax.plot(y, **linestyle(1, label=r'$y = \sqrt{x}$'))

    ax.set_xlim([ -1, 11 ])
    ax.set_ylim([ -1, 4 ])
    ax.grid(True)
    ax.legend()

# --8<--


#--#
