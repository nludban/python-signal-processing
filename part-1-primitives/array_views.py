#!/usr/bin/python3.4

from common import *

# --8<-- examples

## Three views of the same elements
x = numpy.arange(10)		# vector
print('x[2] =', x[2])		# accessed like a list

y = numpy.reshape(x, ( 2, 5 ))	# 2x5 matrix
print('y[1][2] =', y[1][2])	# accessed like nested lists
print('y[1, 2] =', y[1, 2])	# preferred technique

z = y[1,:]			# second row of y
print('z[2] =', z[2])

print('\nBefore:')
print_array(' x=', x)
print_array(' y=', y)
print_array(' z=', z)

## "broadcast" a value to every other element as viewed by z
z[::2] = 42

print('\nAfter:')
print_array(' x=', x)
print_array(' y=', y)
print_array(' z=', z)

# --8<--

#--#
