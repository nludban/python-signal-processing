#!/usr/bin/python3.4
# dot_figure_3.py

from common import *

# --8<-- references

N = 64
scale = 2 / N # 1/N for complex input; 2/N for real
h = [ sinusoid(N, F).real * scale
      for F in range(-N//2, N//2) ]

# --8<--

s3 = sinusoid(N, 3).real
s11 = sinusoid(N, 11).real
s = 1.4 * s3 + -0.8 * s11

# --8<-- generate

S = numpy.array([ numpy.dot(s, h[k]) for k in range(N) ])

# --8<--

n = numpy.arange(-N//2, N//2)

with plot_figure('dot_figure_3.png') as fig:

    ax = fig.add_subplot(1, 1, 1)
    ax.axhline(0, color='black')
    ax.axvline(0, color='black')

    ax.plot(n, S, **linestyle(1))

    ax.set_ylabel(r'$S[k]$')

#--#
