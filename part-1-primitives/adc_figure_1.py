#!/usr/bin/python3.4

from common import *

# Analog
Na = 200
an = numpy.arange(-Na//2, Na//2) * 0.1
a1 = sinusoid(Na, 2).real
a2 = sinusoid(Na, 10).real
a3 = (sinusoid(Na, 5) * numpy.exp(TWO_PI_J * 0.125)).real


# Digital
Nd = 20
dn = numpy.arange(-Nd//2, Nd//2)
d1 = sinusoid(Nd, 2).real
d2 = sinusoid(Nd, 10).real
d3 = (sinusoid(Nd, 5) * numpy.exp(TWO_PI_J * 0.125)).real

with plot_figure('adc_figure_1.png') as fig:

    ax = fig.add_subplot(3, 1, 1)
    ax.plot(an, a1, **linestyle(0))
    ax.stem(dn, d1, **linestyle(1))
    ax.get_xaxis().set_ticks([])
    ax.set_ylim([-1.2, 1.2])

    ax = fig.add_subplot(3, 1, 2)
    ax.plot(an, a2, **linestyle(0))
    ax.stem(dn, d2, **linestyle(1))
    ax.get_xaxis().set_ticks([])
    ax.set_ylim([-1.2, 1.2])

    ax = fig.add_subplot(3, 1, 3)
    ax.plot(an, a3, **linestyle(0))
    ax.stem(dn, d3, **linestyle(1))
    ax.set_ylim([-1.2, 1.2])


#--#
