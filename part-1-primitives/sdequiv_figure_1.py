#!/usr/bin/python3.4
# sdequiv_figure_1.py

from common import *

# Analog sinc
Na = 200
an = numpy.arange(-Na//2, Na//2) * 0.1
ys = sinc(an, 0)

# Digital delta
Nd = 20
dn = numpy.arange(-Nd//2, Nd//2)
yd = delta(dn, 0)

with plot_figure('sdequiv_figure_1.png') as fig:

    ax = fig.add_subplot(1, 1, 1)
    ax.plot(an, ys, **linestyle(1, marker=None))
    ax.stem(dn, yd, **linestyle(2))


#--#
