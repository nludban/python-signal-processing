#!/usr/bin/python3.4
# sinc_figure_1.py

from common import *

N = 200
n = numpy.arange(-N//2, N//2) * 0.1

with plot_figure('sinc_figure_1.png') as fig:

    ax = fig.add_subplot(3, 1, 1)
    ax.axhline(0, color='black')
    ax.axvline(0, color='black')
    ax.plot(n, sinc(n, 0), **linestyle(1, marker=None))
    ax.get_xaxis().set_ticks([])
    ax.set_ylim([-1.2, 1.2])

    # --8<-- generate_1
    y1 = sinc(n, 0)
    y2 = 0.3 * sinc(n, 3)
    y3 = -0.8 * sinc(n, -7)
    # --8<--

    ax = fig.add_subplot(3, 1, 2)
    ax.axhline(0, color='black')
    ax.axvline(0, color='black')
    ax.plot(n, y1, **linestyle(1, marker=None))
    ax.plot(n, y2, **linestyle(2, marker=None))
    ax.plot(n, y3, **linestyle(3, marker=None))
    ax.get_xaxis().set_ticks([])
    ax.set_ylim([-1.2, 1.2])

    # --8<-- generate_2
    y = y1 + y2 + y3
    # --8<--

    ax = fig.add_subplot(3, 1, 3)
    ax.axhline(0, color='black')
    ax.axvline(0, color='black')
    ax.plot(n, y, **linestyle(1, marker=None))
    ax.set_ylim([-1.2, 1.2])


#--#
