#!/usr/bin/python3.4

from common import *


# --8<-- examples

a = numpy.array([ 1, 3, 5, 7 ])
b = numpy.array([ 2, 4, 6, 8 ])

## Element by element
print_array('\na + b =', a + b)
print_array('a * b =', a * b)

## "Broadcast" a scalar to each element
print_array('\na + 5 = ', a + 5)
print_array('12 / b = ', 12 / b)

m = numpy.array([[ 10, 20, 30, 40 ],
                 [ 50, 60, 70, 80 ]])
print_array('\nm =', m)
print_array('m + a =', m + a)

# --8<--

#--#
