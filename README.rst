#######################################################################
README.rst
#######################################################################

=======================================================================
Python Signal Processing Presentations
=======================================================================

A (growing) collection of presentations on using Python for digital
signal processing.

Note the links below do not work if viewing through BitBucket.  See
the Viewing Instructions in the next section.

-----------------------------------------------------------------------
`Part 1 - Primitives <part-1/presentation.html>`_
-----------------------------------------------------------------------

Start here if you're not familiar with NumPy and/or Signal Processing.


=======================================================================
Viewing Instructions
=======================================================================

* Install python3.4, numpy, aiohttp, and docutils.

  * And probably some other stuff...
  * Raspberry Pi instructions RSN...

* Download or clone the project repo.
* Change into a presentation subdirectory (eg, part-1-primitives) and
  run "make" (this may take several minutes on a slow machine).
* Change back to the top level directory and run server.py.

  * usage: server.py [-h HOST] [-p PORT]
  * default: accept connections on any interface, port = 8888

    .. code::

       ======== Running on http://0.0.0.0:8888/ ========
       (Press CTRL+C to quit)

* Point a web browser at the server...
