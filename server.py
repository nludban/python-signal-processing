#!/usr/bin/python3.4

import argparse
import os
import sys

import asyncio
import aiohttp
import aiohttp.web

PARTS = {
    'part-1'	: 'part-1-primitives',
    #part-2
    #...
    }

class IndexView(aiohttp.web.View):

    @asyncio.coroutine
    def get(self):
        #f = open('index.html', 'rb')
        with os.popen('%s ./mkhtml.py README.rst' % sys.executable) as f:
            body = f.read().encode('ascii')
        return aiohttp.web.Response(body=body)

class SharedView(aiohttp.web.View):

    @asyncio.coroutine
    def get(self):
        filename = self.request.match_info['filename']
        f = open(filename, 'rb')
        return aiohttp.web.Response(body=f.read())

class SubDirView(aiohttp.web.View):

    @asyncio.coroutine
    def get(self):
        partN = self.request.match_info['partN']
        subdir = PARTS[partN]
        filename = self.request.match_info['filename']
        #print(filename)
        f = open(os.path.join(subdir, filename), 'rb')
        return aiohttp.web.Response(body=f.read())

ap = argparse.ArgumentParser(
    description='Presentation web server.',
    add_help=False)
ap.add_argument('-h', '--host', dest='host', default='0.0.0.0')
ap.add_argument('-p', '--port', dest='port', type=int, default=8888)
opts = ap.parse_args(sys.argv[1:])

app = aiohttp.web.Application()
app.router.add_route('*', '/', IndexView)
app.router.add_route('*', '/index', IndexView)
app.router.add_route('*', '/index{html}', IndexView)
app.router.add_route('*', '/{filename}', SharedView)
app.router.add_route('*', '/{partN}/{filename}', SubDirView)

aiohttp.web.run_app(app, host=opts.host, port=opts.port)

#--#
