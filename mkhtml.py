#!/usr/bin/python3.4

import os
import re
import sys

from examples import html_parts

import pygments
import pygments.lexers
import pygments.formatters

def hilite_python_div(code):
    div = pygments.highlight(code,
                             pygments.lexers.python.PythonLexer(),
                             pygments.formatters.HtmlFormatter())
    return div

def hilite_rst_div(doc):
    div = pygments.highlight(doc,
                             pygments.lexers.RstLexer(),
                             pygments.formatters.HtmlFormatter())
    return div

TMPL = r'''<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="@@SUMMARY@@" />
    <meta name="author" content="@@AUTHOR@@" />
    <link rel="stylesheet" type="text/css" href="../html4css1.css" />
    <link rel="stylesheet" type="text/css" href="../presentation.css" />
    <title>@@TITLE@@</title>
</head>
<body>
  @@CONTENTS@@
</body>
</html>
'''

if len(sys.argv) == 1:
    # pygmentize -S default -f html > pygments.css
    from pygments.formatters import HtmlFormatter
    # CSS selectors: .highlight ...
    help(HtmlFormatter().get_style_defs)
    print(HtmlFormatter().get_style_defs('.code'))
    sys.exit(0)

arg0, presentation_txt = sys.argv
src = open(presentation_txt).read()

while True:
    m = re.search(r'([ \t]+)@@include\s+([^\n]+)', src)
    if not m:
        assert '@include' not in src
        break
    print('Found: %r' % m.group(), file=sys.stderr)
    a, b = m.start(), m.end()
    prefix = m.group(1) #.expandtabs()
    args = m.group(2).split()
    src_a = src[:a]
    src_b = src[b:]

    code_py, tag = args
    code = open(code_py).read()
    m = re.search(r'# --8<-- %s' % tag, code)
    code = code[m.end():].lstrip('\n')
    m = re.search(r'# --8<--', code)
    code = code[:m.start()].rstrip()

    src = (src_a
           + '\n'.join([ prefix + line
                         for line in code.splitlines() ])
           + src_b)

while True:
    m = re.search(r'([ \t]+)@@run\s+(.+)', src)
    if not m:
        break
    print('Found: %r' % m.group(), file=sys.stderr)
    a, b = m.start(), m.end()
    prefix = m.group(1) #.expandtabs()
    args = m.group(2).split()
    src_a = src[:a]
    src_b = src[b:]

    code_py, *args = args
    with os.popen('%s %s %s' % ( sys.executable,
                                 code_py,
                                 ' '.join(args) )) as f:
        output = f.readlines()

    src = (src_a
           + '\n'.join([ prefix + line.rstrip()
                         for line in output ])
           + src_b)

print('Processed Input:', file=sys.stderr)
lineno = 1
for line in src.splitlines():
    #print('%4i: %s' % ( lineno, line.rstrip().expandtabs() ),
    #      file=sys.stderr)
    print('%4i: %r' % ( lineno, line.rstrip() ),
          file=sys.stderr)
    lineno += 1

parts = html_parts(src, input_encoding='utf8',
                   initial_header_level=1)
contents = parts['fragment']
#print(sorted(parts.keys()))

if 'System Message: ERROR' in contents:
    sys.exit(1)

assert '@include' not in contents
print(TMPL.replace('@@CONTENTS@@', contents))



#--#
